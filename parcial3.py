
import speech_recognition as sr
import firebase_admin
from firebase_admin import credentials
from firebase_admin import db
from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image
from PIL import ImageTk
import time

window_width=1920
window_height=1080
canvas_width =  1900
canvas_height = 980
sizex_circ=60
sizey_circ=60
width = 150
height = 150
cuenta=[]
contador=[]
datos = []
suma=0
numero=1
content=""



placa = Arduino ('COM3')
it = util.Iterator(placa)
it.start()
led=placa.get_pin("d:9:p")
led2=placa.get_pin("d:10:p")
lectura=placa.get_pin("a:0:i")
lectura1=placa.get_pin("a:1:i")

root=Tk()
root.config(bg="dark green")
root.geometry("400x400+0+0")



cred = credentials.Certificate('llave/tercercorte.json')
# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://tercercorte-bd4f4.firebaseio.com/'
})






comando = StringVar()


texto = Label(root, text="interfaz", bg='cadet blue1', font=("Arial Bold", 14), fg="white")
texto.place(x=10, y=20)

jose = Label(root, text="ADC1", bg='red2', font=("Arial Bold", 14), fg="white")
jose.place(x=10, y=50)

linea = Label(root, text="interfaz", bg='cadet blue1', font=("Arial Bold", 14), fg="white")
linea.place(x=10, y=90)

luis = Label(root, text="ADC2", bg='red2', font=("Arial Bold", 14), fg="white")
luis.place(x=10, y=120)



luis1 = Label(root, text="", bg='dark green', font=("Arial Bold", 14), fg="white")
luis1.place(x=10, y=200)



luis2 = Label(root, text="", bg='dark green', font=("Arial Bold", 14), fg="white")
luis2.place(x=10, y=250)






def entrada (input): 
    global content
    content = dato.get()
    print(content)
    dato.delete(0, END)


    

    

                
       
    

promedio1=0
promedio2=0       
    
Arreglo1=[]
Arreglo2=[]



def update_label():
    global cuenta, promedio1,promedio2,j,l
    j=lectura.read()
    ref = db.reference("adc")
    ref.update({
        'adc1': {
            'valor': (j)  
            }

            }) 
                    
    l=lectura1.read()
    ref = db.reference("adc")
    ref.update({
        'adc2': {
            'valor': (l)  
            }

            }) 
    
    
    texto.configure(text=str(j))
    linea.configure(text=str(l))
  
    
    
    
    jose.configure(text="ADC1")
    luis.configure(text="ADC2")
   
    led.write(0)
    led2.write(0)
    
    print ("content",content)

    if content== "I" or content=="i":
        led.write(j)
        led2.write(l)
        print("se ajustaron los leds")
        luis1.configure(text=("se ajustaron los leds"))
    if content== "P" or content=="p":
        if len(Arreglo1)<=9:
            Arreglo1.append(j)
            Arreglo2.append(l)
            if (len(Arreglo1)==10):

                for i in range(0,9,1):
                    promedio1=promedio1+Arreglo1[i]
                    promedio2=promedio2+Arreglo2[i]
                promedio1=promedio1/10
                promedio2=promedio2/10
                print (Arreglo1)
                print (Arreglo2)
                print (promedio1)
                print (promedio2)
                luis1.configure(text="p1:"+str(promedio1))
                ref = db.reference("adc")
                ref.update({
                    'promedio1': {
                        'valor': (promedio1)  
                    }

                    }) 
                luis2.configure(text="p2:"+str(promedio2))
                ref = db.reference("adc")
                ref.update({
                    'promedio2': {
                        'valor': (promedio2)  
                    }

                    }) 
    root.after(200,update_label)
    




your_label=Label(root)
your_label.pack()
start_button=Button(root,text="start",command=update_label)
start_button.place(x=40,y=300)

stop=Button(root,text="salir",command=root.destroy)
stop.place(x=40,y=350)

Label(root, text="Input: ").place(x=230, y=300)
dato = Entry(root)
dato.bind('<Return>',entrada) 
dato.place(x=200, y=340)

e=Label(root,text="")
img4 = Image.open("C:/Users/JOSE LUIS/Pictures/Saved Pictures/logo.png")
img4 = img4.resize((200,50))
photoImg4=  ImageTk.PhotoImage(img4)

e.configure(image=photoImg4)
e.place(x=180,y=0)

cont = db.reference('adc/adc1/valor').get()
print(cont)
cont2 = db.reference('adc/adc2/valor').get()
print(cont2)

cont3 = db.reference('adc/promedio1/valor').get()
print(cont3)
cont4 = db.reference('adc/promedio2/valor').get()
print(cont4)



root.mainloop()

